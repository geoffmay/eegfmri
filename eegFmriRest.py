# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

#imports
from __future__ import absolute_import, division
import os
import sys
import json
from datetime import datetime
from collections import namedtuple

# numpy libraries
import numpy as np
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle

# psychopy libraries
import psychopy as psy
from psychopy import locale_setup
from psychopy import prefs
from psychopy import logging
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
from psychopy.hardware import keyboard

# eyetracker
from psychopy.iohub import launchHubServer
from psychopy.core import getTime, wait


class EegFmri:

    lastStim = None
    stims = []
    win = None
    io = None
    eyetrackerFileHandle = None

    # helper functions
    def unusedSubjectId(self, dataFolder, subjectRootName):
        # ensure root name ends with an underscore
        #if(subjectRootName[-1] != -1):
        #    subjectRootName = subjectRootName + '_'
        # find the first number that hasn't been used yet
        subjectNumber = 0
        files = os.listdir(dataFolder)
        used = True
        while(used):
            subjectNumber = subjectNumber + 1
            subjectName = subjectRootName + '%03d' % subjectNumber
            used = False
            for fileNumber in range(len(files)):
                if(files[fileNumber].startswith(subjectName)):
                    used = True
                    break
        return subjectName

    def createStim(self, text, name, win):
        stim = visual.TextStim(win=win, name=name,
                                    text=text,
                                    font='Arial',
                                    pos=(0, 0), height=0.1, wrapWidth=None, ori=0,
                                    color='white', colorSpace='rgb', opacity=1,
                                    languageStyle='LTR',
                                    depth=0.0)
        self.stims.append(stim);
        return stim

    def timeString():
        time = datetime.now();
        timeStr = time.isoformat()
        timeStr = timeStr.replace(':', '-')
        timeStr = timeStr.replace('T', '-')
        timeStr = timeStr.replace('.', '-')
        return timeStr

    def namedTupleToJson(tuple):
        result = '{';
        count = len(tuple._fields)
        for i in range(count):
            result = result + '"' + tuple._fields[i] + '": '
            result = result + '"' + str(tuple[i]) + '"'
            if(i < count):
                result = result + ', ';
        
        result = result + '}';




    def drawStim(self, stimulus, frameN, t, win, tThisFlipGlobal):
        if(self.lastStim):
            self.lastStim.setAutoDraw(False)
        # *crosshair* updates
        # if crosshair.status == NOT_STARTED and tThisFlip >= 0.0 - frameTolerance:
        # keep track of start time/frame for later
        stimulus.frameNStart = frameN  # exact frame index
        stimulus.tStart = t  # local t and not account for scr refresh
        stimulus.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(stimulus, 'tStartRefresh')  # time at next scr refresh
        stimulus.setAutoDraw(True)
        self.eyetrackerFileHandle.write('{"type": "stimulus", "text": "' + stimulus.text + '", "time": "' + self.timeString() + '"}\n')
        self.lastStim = stimulus

    def runTrial(self, dataFolder, subjectName, runNumber, condition, calibrateEyetracker, needsBreak):
        # Store info about the experiment session
        psychopyVersion = '2020.1.1'
        runDesc = 'run%02d_%s' % (runNumber, condition)
        expInfo = {'participant': subjectName, 'run': runDesc}
        dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title='Enter information for the run about to be recorded', screen=1)
        if not dlg.OK:
            core.quit()  # user pressed cancel
        expInfo['date'] = data.getDateStr()  # add a simple timestamp
        expInfo['subjectId'] = subjectName
        expInfo['psychopyVersion'] = psychopyVersion

        expName = '%s_%s' % (expInfo['subjectId'], runDesc)


        # Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
        filename = dataFolder + os.sep + '%s_%s' % (expName, self.timeString())
        eyetrackerFilename = filename + '_eyetracker.json'
        self.eyetrackerFileHandle = open(eyetrackerFilename, "a")
        self.eyetrackerFileHandle.write('{\n')


        # An ExperimentHandler isn't essential but helps with data saving
        thisExp = data.ExperimentHandler(name=expName, version='',
                                         extraInfo=expInfo, runtimeInfo=None,
                                         originPath=os.path.abspath(__file__),
                                         savePickle=True, saveWideText=True,
                                         dataFileName=filename)
        # save a log file for detail verbose info
        logFile = logging.LogFile(filename + '.log', level=logging.EXP)
        logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

        endExpNow = False  # flag for 'escape' or other condition => quit the exp
        frameTolerance = 0.001  # how close to onset before 'same' frame

        # Start Code - component code to be run before the window creation

        # Get the eye tracker device. (from: https://www.psychopy.org/api/iohub/device/eyetracker_interface/SR_Research_Implementation_Notes.html)
        eyetrackerPresent = True

        #        if not hasattr(self.io.devices, 'tracker'):
        #            eyetrackerPresent = False
        #        else:
        #        try:
        tracker = self.io.devices.tracker
        connected = tracker.setConnectionState(True)
        #        if(connected):
        print('Starting eyetracker recording');
        if(calibrateEyetracker):
            # start recording eyetracker data
            # run eyetracker calibration
            print('Starting eyetracker calibration')
            r = tracker.runSetupProcedure()
            print('Finished eyetracker calibration')
           
        else:
            print('skipping eyetracker calibration')
            #                else:
            #                    eyetrackerPresent = False
        #        except AttributeError:
        #            eyetrackerPresent = False
        if not eyetrackerPresent:
            print('WARNING: eyetracker was not found')

        # ------Prepare to start Routine "trial"-------
        continueRoutine = True
        
        
        # Setup the Window
        win = visual.Window(
            size=(1024, 768), fullscr=True, screen=0,
            #size=(1024, 768), fullscr=True, screen=1,
            winType='pyglet', allowGUI=False, allowStencil=False,
            #    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            monitor='projector', color=[0, 0, 0], colorSpace='rgb',
            blendMode='avg', useFBO=True,
            units='height')

        # define stimuli for trials
        crosshair = self.createStim(self, '+', 'crosshair', win)
        restInstructions = self.createStim(self, 'Please hold very still and look at the plus sign on the screen.', 'restInstructions', win)
        anatInstructions = self.createStim(self, 'You can close your eyes, but try to hold very still.', 'restInstructions', win)

        breatheInstructions1 = self.createStim(self, 'When you see "BREATE IN", inhale slowly; "BREATHE OUT" means exhale slowly. ', 'breatheInstructions1', win)
        breatheInstructions2 = self.createStim(self, 'If you cannot breathe in or out any more, just hold where you are until the instructions change.', 'breatheInstructions2', win)
        breatheInstructions3 = self.createStim(self, 'If you see "HOLD", hold your breath. "+" means breathe however you want.', 'breatheInstructions3',  win)
        breatheInstructions4 = self.createStim(self, 'If you become uncomfortable, just breathe normally.', 'breatheInstructions4',  win)
        breatheIn = self.createStim(self, 'BREATHE IN', 'breatheIn', win)
        breatheOut = self.createStim(self, 'BREATHE OUT', 'breatheOut', win)
        hold = self.createStim(self, 'HOLD', 'hold', win)

        nextSoon = self.createStim(self, 'Well done! The next run will start soon...', 'nextSoon',  win)
        breakTime = self.createStim(self, 'It is time for a break. We will see you soon.', 'breakTime',  win)



        # store frame rate of monitor if we can measure it
        expInfo['frameRate'] = win.getActualFrameRate()
        if expInfo['frameRate'] is not None:
            frameDur = 1.0 / round(expInfo['frameRate'])
        else:
            frameDur = 1.0 / 60.0  # could not measure, so guess

        # create a default keyboard (e.g. to check for escape)
        defaultKeyboard = keyboard.Keyboard()

        # Initialize components for Routine "trial"
        trialClock = core.Clock()


        # Create some handy timers
        globalClock = core.Clock()  # to track the time since experiment started
        routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine



        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1

        expectedTriggerPulseCount = 132
        triggerPulseCount = 0

        print('Displaying instructions. Press any key to continue');

        # Show instructions
        t = trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        if (condition == 'rest'):
            instructions = [restInstructions]
            self.drawStim(self, restInstructions, frameN, t, win, tThisFlipGlobal)
        elif (condition == 'breathe'):
            instructions = [breatheInstructions1, breatheInstructions2, breatheInstructions3, breatheInstructions4]
        else:
            instructions = [restInstructions]
        getStarted = False
        instructionIndex = 0
        self.drawStim(self, instructions[instructionIndex], frameN, t, win, tThisFlipGlobal)
        while(not getStarted):
            win.flip()
            keys = defaultKeyboard.getKeys()
            first = True
            for key in keys:
                if (key.name == 'escape'):
                    getStarted = True
                    continueRoutine = False
                elif (key.name == 'equal'):
                    getStarted = True
                elif(first):
                    first = False
                    instructionIndex = instructionIndex + 1;
                    if(instructionIndex >= len(instructions)):
                        getStarted = True
                    else:
                        self.drawStim(self, instructions[instructionIndex], frameN, t, win, tThisFlipGlobal)

        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        tracker.setRecordingState(True)
        self.drawStim(self, crosshair, frameN, t, win, tThisFlipGlobal)

        # Run the experiment
        eyeTrackerEventType = 'namedtuple'
        print('Starting the experiment. Press Press f to finish the run, Esc to stop the experiment');
        while continueRoutine:
            # get current time
            t = trialClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=trialClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)



            if False:
                eyeinfo = tracker.getLastSample(asType=eyeTrackerEventType)
                self.eyetrackerFileHandle.write(str(type(eyeinfo)) + '\n')
                if str(type(eyeinfo)) == eyeTrackerEventType:
                    #if isinstance(eyeinfo,list)
                    #print('eyeinfo:' + json.dumps(eyeinfo) + ',\n') 
                    # self.eyetrackerFileHandle.write(str(type(eyeinfo)))
                    # self.eyetrackerFileHandle.write(json.dumps(eyeinfo) + ',\n')
                    self.eyetrackerFileHandle.write(self.namedTupleToJson(eyeinfo) + ',\n')

            if True:
                trackerEvents = tracker.getEvents(asType=eyeTrackerEventType)
                for e in trackerEvents:
                    self.eyetrackerFileHandle.write(str(type(e)) + '\n')
                    self.eyetrackerFileHandle.write(json.dumps(e) + ',\n')
                
    
            # end the routine if the endExpNowFlag is set=
            if endExpNow:
                continueRoutine = False

            # check keyboard input
            keys = defaultKeyboard.getKeys()
            for key in keys:
                if(key.name == 'escape'):
                    continueRoutine = False
                elif(key.name == 'f'):
                    print('manually finishing run #' + str(runNumber))
                    continueRoutine = False
                elif(key.name == 'equal'):
                    triggerPulseCount = triggerPulseCount + 1
                    self.eyetrackerFileHandle.write('{"type": "mriTrigger", "number": "'+ str(triggerPulseCount) + '", "time": "' + self.timeString() + '"}\n')
                    print('received trigger pulse #' + str(triggerPulseCount) + ' of ' + str(expectedTriggerPulseCount))
                    if (condition == 'demo'):
                        stimIndex = triggerPulseCount % len(self.stims)
                        stim = self.stims[stimIndex]
                        self.drawStim(self, stim, frameN, t, win, tThisFlipGlobal)
                    elif (condition == 'rest'):
                        dummy = 1
                    elif (condition == 'breathe'):
                        if(triggerPulseCount <= 120):
                            pulsePhase =triggerPulseCount % 15
                            if(1 <= pulsePhase and pulsePhase <= 7):
                                parity = pulsePhase % 2
                                if(parity == 1):
                                    self.drawStim(self, breatheIn, frameN, t, win, tThisFlipGlobal)
                                else:
                                    self.drawStim(self, breatheOut, frameN, t, win, tThisFlipGlobal)
                            elif(8 <= pulsePhase and pulsePhase <= 11):
                                self.drawStim(self, hold, frameN, t, win, tThisFlipGlobal)
                            else:
                                self.drawStim(self, crosshair, frameN, t, win, tThisFlipGlobal)
                        else:
                            self.drawStim(self, crosshair, frameN, t, win, tThisFlipGlobal)
                    # end switch for condition type
                # end conditional for processing "=" mri trigger pulse
            # end loop for processing keyboard buffer

            if (triggerPulseCount >= expectedTriggerPulseCount):
                print('run #' + str(runNumber) + ' finished')
                continueRoutine = False

            # refresh the screen (requires auto-draw to be set, otherwise the screen will be blank)
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        # end "continueRoutine" loop

        if needsBreak:
            print('time for a break and impedance check')

        #        if needsBreak:
        #            self.drawStim(self, breakTime, frameN, t, win, tThisFlipGlobal)
        #        else:
        #            self.drawStim(self, nextSoon , frameN, t, win, tThisFlipGlobal)

        routineTimer.reset()

        # Flip one final time so any remaining win.callOnFlip()
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()

        # Stop the eyetracker
        print('Stopping eyetracker recording')
        tracker.setRecordingState(False)


        self.eyetrackerFileHandle.close()


        # close experiment (relies on auto-save)
        # thisExp.saveAsWideText(filename + '.csv')
        # thisExp.saveAsPickle(filename)
        thisExp.close();
        print('Data saved to ' + filename)

        #wait(10)

        win.close()
        logging.flush()
    # end function runTrial



    # driver for demonstration of all stimuli
    def runSession(self):

        # Change to the directory this script is in and generate an unused subject id
        _thisDir = os.path.dirname(os.path.abspath(__file__))
        os.chdir(_thisDir)
        dataFolder = os.path.join(_thisDir, 'data')
        subjectId = self.unusedSubjectId(self, dataFolder, 'EEGFMRI')

        # Define the order of trials
        conditions = ['demo']

        calibrateEyetracker = [True]

        needsBreak = [True]

        for i in range(len(conditions)):
            self.runTrial(self, dataFolder, subjectId, i, conditions[i], calibrateEyetracker[i], needsBreak[i])

        # make sure everything is closed down
        # thisExp.abort()  # or data files will save again on exit
        core.quit()

    # end function runDemo

    # main driver for the experiment
    def runSession(self):

        iohub_config = {'eyetracker.hw.sr_research.eyelink.EyeTracker':
                            {'name': 'tracker',
                             'model_name': 'EYELINK 1000 DESKTOP',
                             'runtime_settings': {'sampling_rate': 500,
                                                  'track_eyes': 'RIGHT'}
                             }
                        }
        self.io = launchHubServer(**iohub_config)

        # Change to the directory this script is in and generate an unused subject id
        _thisDir = os.path.dirname(os.path.abspath(__file__))
        os.chdir(_thisDir)
        dataFolder = os.path.join(_thisDir, 'data')
        subjectId = self.unusedSubjectId(self, dataFolder, 'EEGFMRI')



        # Define the order of trials
        conditions = ['breathe', 'breathe', 'breathe', 'breathe']

        calibrateEyetracker = [True, True, True, True]

        needsBreak = [False, False, False, True]

        for i in range(len(conditions)):
            self.runTrial(self, dataFolder, subjectId, i + 1, conditions[i], calibrateEyetracker[i], needsBreak[i])

        self.eyetrackerFileHandle.write('{\n')


        self.io.quit()


        # make sure everything is closed down
        # thisExp.abort()  # or data files will save again on exit
        core.quit()

    # end function runSession

        #
        #
        #
        #
        # =====
        # def print_hi(name):
        #     # Use a breakpoint in the code line below to debug your script.
        #     print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.
        #
        #
        # # Press the green button in the gutter to run the script.
        # if __name__ == '__main__':
        #     print_hi('PyCharm')
        #
        # # See PyCharm help at https://www.jetbrains.com/help/pycharm/
        # a = np.zeros([4,4])
        # print(a);


if __name__ == "__main__":
    eegFmri = EegFmri
    eegFmri.runSession(eegFmri)
